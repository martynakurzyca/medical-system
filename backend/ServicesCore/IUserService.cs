﻿using MedicalSystemBackend.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalSystemBackend.ServicesCore
{
    public interface IUserService
    {
        int GetUserIdByLoginAndPassword(string login, string password);
        UserDataDTO GetUserDataByUserId(int userId);
        UserDTO GetUserByUserId(int userId);
        UserListDTO GetAllUsers();
        void AddUser(UserDTO newUserDTO);
        void UpdateUser(UserDTO newUserDTO, int userId);
        void DeactivateUser(int userId);
        void UpdateUserPassword(UserPasswordChangeDTO userPasswordChangeDTO, int userId);
        bool CheckIfNewUserLoginIsUnique(string newUserLogin);
        bool CheckIfUpdatedUserLoginIsUnique(string updatedUserLogin, int userId);
        int GetRegistrantIdByUserId(int userId);
        int GetDoctorIdByUserId(int userId);
        int GetLaboratorySupervisorIdByUserId(int userId);
        int GetLaboratoryAssistantIdByUserId(int userId);
        bool IsAdmin(int userId);
        bool IsDoctor(int userId);
        bool IsRegistrant(int userId);

    }
}
