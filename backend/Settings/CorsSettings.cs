﻿namespace MedicalSystemBackend.Settings
{
    public class CorsSettings
    {
        public string[] Origins { get; set; }
    }
}