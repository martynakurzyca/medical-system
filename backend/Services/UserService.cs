﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.Model;
using MedicalSystemBackend.Repositories;
using MedicalSystemBackend.ServicesCore;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;

namespace MedicalSystemBackend.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<UserDAO> usersRepository;
        private readonly IRepository<RegistrantDAO> registrantsRepository;
        private readonly IRepository<DoctorDAO> doctorsRepository;
        private readonly IRepository<LaboratoryAssistantDAO> laboratoryAssistantsRepository;
        private readonly IRepository<LaboratorySupervisorDAO> laboratorySupervisorsRepository;
        private readonly IRepository<AdminDAO> adminsRepository;
        private readonly IRepository<UserRoleDAO> rolesRepository;
        private readonly PasswordHasher passwordHasher;

        public UserService(IRepository<UserDAO> usersRepository,
            IRepository<RegistrantDAO> registrantsRepository,
            IRepository<DoctorDAO> doctorsRepository,
            IRepository<LaboratoryAssistantDAO> laboratoryAssistantsRepository,
            IRepository<LaboratorySupervisorDAO> laboratorySupervisorsRepository,
            IRepository<UserRoleDAO> rolesRepository,
            IRepository<AdminDAO> adminsRepository)
        {
            this.usersRepository = usersRepository;
            this.registrantsRepository = registrantsRepository;
            this.doctorsRepository = doctorsRepository;
            this.laboratoryAssistantsRepository = laboratoryAssistantsRepository;
            this.laboratorySupervisorsRepository = laboratorySupervisorsRepository;
            this.rolesRepository = rolesRepository;
            this.adminsRepository = adminsRepository;
            this.passwordHasher = new PasswordHasher();

        }
        public int GetUserIdByLoginAndPassword(string login, string password)
        {
            IEnumerable<UserDAO> users = usersRepository.GetAll();
            foreach (UserDAO user in users)
            {
                if (user.Login == login)
                {
                    if (user.IsActive == true)
                    {
                        if (passwordHasher.VerifyHashedPassword(user.Password, password) == Microsoft.AspNet.Identity.PasswordVerificationResult.Success)
                        {
                            return user.Id;
                        }
                        else if (passwordHasher.VerifyHashedPassword(user.Password, password) == Microsoft.AspNet.Identity.PasswordVerificationResult.SuccessRehashNeeded)
                        {
                            throw new MedicalSystemServerException("Haslo powinno byc rehashowane");
                        }
                        else
                        {
                            throw new MedicalSystemServerException("Niepoprawne haslo");
                        }
                    }
                    else
                    {
                        throw new MedicalSystemServerException("Uzytkownik o tym loginie nie jest aktywny");
                    }
                }
            }
            throw new MedicalSystemServerException("Niepoprawny login");
        }
        public void AddUser(UserDTO newUserDTO)
        {
            if (!CheckIfNewUserLoginIsUnique(newUserDTO.Login))
            {
                throw new MedicalSystemServerException("Nowy login uzytkownika nie jest unikalny");
            }
            UserDAO newUserDAO = new UserDAO
            {
                Login = newUserDTO.Login,
                FirstName = newUserDTO.FirstName,
                Surname = newUserDTO.Surname,
                RoleId = newUserDTO.Role.Id,
                IsActive = newUserDTO.IsActive
            };
            newUserDAO.Password = passwordHasher.HashPassword(newUserDTO.Password);
            usersRepository.Add(newUserDAO);
            if (newUserDAO.RoleId == 1) doctorsRepository.Add(new DoctorDAO
            {
                UserId = GetUserIdByLoginAndPassword(newUserDAO.Login, newUserDTO.Password),
                LicenceNumber = newUserDTO.LicenceNumber
            });
            else if (newUserDAO.RoleId == 2)
            {
                registrantsRepository.Add(new RegistrantDAO
                {
                    UserId = GetUserIdByLoginAndPassword(newUserDAO.Login, newUserDTO.Password)
                });
            }
            else if (newUserDAO.RoleId == 3)
            {
                laboratoryAssistantsRepository.Add(new LaboratoryAssistantDAO
                {
                    UserId = GetUserIdByLoginAndPassword(newUserDAO.Login, newUserDTO.Password)
                });
            }
            else if (newUserDAO.RoleId == 4)
            {
                laboratorySupervisorsRepository.Add(new LaboratorySupervisorDAO
                {
                    UserId = GetUserIdByLoginAndPassword(newUserDAO.Login, newUserDTO.Password)
                });
            }
            else if (newUserDAO.RoleId == 5)
            {
                adminsRepository.Add(new AdminDAO
                {
                    UserId = GetUserIdByLoginAndPassword(newUserDAO.Login, newUserDTO.Password)
                });
            }
            else
            {
                throw new MedicalSystemServerException("Nie poprawna roleId");
            }
        }

        public void UpdateUser(UserDTO updatedUserDTO, int userId)
        {
            if (!CheckIfUpdatedUserLoginIsUnique(updatedUserDTO.Login, userId))
            {
                throw new MedicalSystemServerException("Zaktualizowany login uzytkownika nie jest unikalny");
            }
            UserDAO updatedUserDAO = new UserDAO
            {
                Id = userId,
                Login = updatedUserDTO.Login,
                FirstName = updatedUserDTO.FirstName,
                Surname = updatedUserDTO.Surname,
                RoleId = updatedUserDTO.Role.Id,
                IsActive = updatedUserDTO.IsActive
            };
            if (updatedUserDTO.Password != null)
            {
                updatedUserDAO.Password = passwordHasher.HashPassword(updatedUserDTO.Password);
            }
            else
            {
                updatedUserDAO.Password = usersRepository.Get(userId).Password;
            }
            usersRepository.Update(updatedUserDAO);

            if (IsDoctor(userId))
            {
                int doctorId = GetDoctorIdByUserId(userId);
                DoctorDAO updatedDoctorDAO = new DoctorDAO
                {
                    Id = doctorId,
                    UserId = userId,
                    LicenceNumber = updatedUserDTO.LicenceNumber
                };
                doctorsRepository.Update(updatedDoctorDAO);
            }
        }

        public void UpdateUserPassword(UserPasswordChangeDTO userPasswordChangeDTO, int userId)
        {
            UserDAO userDAO = usersRepository.Get(userId);
            if (passwordHasher.VerifyHashedPassword(userDAO.Password, userPasswordChangeDTO.OldPassword) == Microsoft.AspNet.Identity.PasswordVerificationResult.Success)
            {
                userDAO.Password = passwordHasher.HashPassword(userPasswordChangeDTO.NewPassword);
            }
            else
            {
                throw new MedicalSystemServerException("Poprzednie haslo nie jest poprawne");
            }
            usersRepository.Update(userDAO);
        }

        public bool CheckIfNewUserLoginIsUnique(string newUserLogin)
        {
            IEnumerable<UserDAO> users = usersRepository.GetAll();
            foreach (UserDAO user in users)
            {
                if (user.Login == newUserLogin)
                {
                    return false;
                }
            }
            return true;
        }

        public bool CheckIfUpdatedUserLoginIsUnique(string updatedUserLogin, int userId)
        {
            if (usersRepository.Get(userId).Login == updatedUserLogin) return true;
            IEnumerable<UserDAO> users = usersRepository.GetAll();
            foreach (UserDAO user in users)
            {
                if (user.Login == updatedUserLogin)
                {
                    return false;
                }
            }
            return true;
        }

        public UserDataDTO GetUserDataByUserId(int userId)
        {
            UserDAO userDAO = usersRepository.Get(userId);
            UserDataDTO userDataDTO = new UserDataDTO
            {
                Firstname = userDAO.FirstName,
                Surname = userDAO.Surname,
                Role = rolesRepository.Get(userDAO.RoleId).Name
            };
            return userDataDTO;
        }

        //przerobic na generyczne!!!
        public int GetRegistrantIdByUserId(int userId)
        {
            IEnumerable<RegistrantDAO> registrants = registrantsRepository.GetAll();
            foreach (RegistrantDAO registrant in registrants)
            {
                if (registrant.UserId == userId)
                {
                    return registrant.Id;
                }
            }
            throw new MedicalSystemServerException("Rejestrator o tym userId nie istnieje");
        }
        public int GetDoctorIdByUserId(int userId)
        {
            IEnumerable<DoctorDAO> doctors = doctorsRepository.GetAll();
            foreach (DoctorDAO doctor in doctors)
            {
                if (doctor.UserId == userId)
                {
                    return doctor.Id;
                }
            }
            throw new MedicalSystemServerException("Doktor o tym userId nie istnieje");
        }
        public int GetLaboratorySupervisorIdByUserId(int userId)
        {
            IEnumerable<LaboratorySupervisorDAO> laboratorySupervisors = laboratorySupervisorsRepository.GetAll();
            foreach (LaboratorySupervisorDAO laboratorySupervisor in laboratorySupervisors)
            {
                if (laboratorySupervisor.UserId == userId)
                {
                    return laboratorySupervisor.Id;
                }
            }
            throw new MedicalSystemServerException("Kierownik laboratorium o tym userId nie istnieje");
        }
        public int GetLaboratoryAssistantIdByUserId(int userId)
        {
            IEnumerable<LaboratoryAssistantDAO> laboratoryAssistants = laboratoryAssistantsRepository.GetAll();
            foreach (LaboratoryAssistantDAO laboratoryAssistant in laboratoryAssistants)
            {
                if (laboratoryAssistant.UserId == userId)
                {
                    return laboratoryAssistant.Id;
                }
            }
            throw new MedicalSystemServerException("Laborant o tym userId nie istnieje");
        }

        public bool IsAdmin(int userId)
        {
            IEnumerable<AdminDAO> admins = adminsRepository.GetAll();
            foreach (AdminDAO admin in admins)
            {
                if (admin.UserId == userId)
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsDoctor(int userId)
        {
            IEnumerable<DoctorDAO> doctors = doctorsRepository.GetAll();
            foreach (DoctorDAO doctor in doctors)
            {
                if (doctor.UserId == userId)
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsRegistrant(int userId)
        {
            IEnumerable<RegistrantDAO> registrants = registrantsRepository.GetAll();
            foreach (RegistrantDAO registrant in registrants)
            {
                if (registrant.UserId == userId)
                {
                    return true;
                }
            }
            return false;
        }

        public void DeactivateUser(int userId)
        {
            UserDAO userDAO = usersRepository.Get(userId);
            if (userDAO.IsActive)
            {
                userDAO.IsActive = false;
                usersRepository.Update(userDAO);
            }
            else
            {
                throw new MedicalSystemServerException("Niepowodzenie, uzytkownik jest juz deazaktywowany");
            }
        }

        public UserDTO GetUserByUserId(int userId)
        {
            UserDAO userDAO = usersRepository.Get(userId);
            UserDTO userDTO = new UserDTO
            {
                Id = userDAO.Id,
                FirstName = userDAO.FirstName,
                Surname = userDAO.Surname,
                Login = userDAO.Login,
                IsActive = userDAO.IsActive,
                Role = new RoleDTO
                {
                    Id = rolesRepository.Get(userDAO.RoleId).Id,
                    Name = rolesRepository.Get(userDAO.RoleId).Name
                }
            };
            if (IsDoctor(userId))
            {
                userDTO.LicenceNumber = doctorsRepository.Get(GetDoctorIdByUserId(userId)).LicenceNumber;
            }
            return userDTO;
        }

        public UserListDTO GetAllUsers()
        {
            UserListDTO userListDTO = new UserListDTO();
            IEnumerable<UserDAO> users = usersRepository.GetAll();
            foreach (UserDAO user in users)
            {
                UserDTO userDTO = new UserDTO
                    {
                    Id = user.Id,
                        FirstName = user.FirstName,
                        Surname = user.Surname,
                        IsActive = user.IsActive,
                        Login = user.Login,
                        Role = new RoleDTO
                        {
                            Id = user.RoleId,
                            Name = rolesRepository.Get(user.RoleId).Name
                        }
                    };
                if (IsDoctor(user.Id))
                {
                    userDTO.LicenceNumber = doctorsRepository.Get(GetDoctorIdByUserId(user.Id)).LicenceNumber;
                }
                userListDTO.Users.Add(userDTO);
            }
            return userListDTO;
        }

    }
}