﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.Model;
using MedicalSystemBackend.Repositories;
using MedicalSystemBackend.ServicesCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalSystemBackend.Services
{
    public class UserRoleService: IUserRoleService
    {
        private readonly IRepository<UserRoleDAO> rolesRepository;

        public UserRoleService(IRepository<UserRoleDAO> rolesRepository)
        {
            this.rolesRepository = rolesRepository;
        }

        public RoleListDTO GetRoles()
        {
            IEnumerable<UserRoleDAO> roles = rolesRepository.GetAll();
            RoleListDTO roleListDTO = new RoleListDTO();
            foreach(UserRoleDAO role in roles)
            {
                roleListDTO.Roles.Add(new RoleDTO 
                { 
                    Id = role.Id, 
                    Name = role.Name 
                });
            }
            return roleListDTO;
        }
    }
}
