﻿using System.Collections.Generic;

namespace MedicalSystemBackend.Model
{
    public class DoctorDAO
    {
        public int Id { get; set; }
        public string LicenceNumber { get; set; }

        public int UserId { get; set; }
        public UserDAO User { get; set; }
        public List<AppointmentDAO> Appointments { get; set; }
    }
}
