﻿using System.Collections.Generic;

namespace MedicalSystemBackend.Model
{
    public class LaboratoryTestStatusDAO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<LaboratoryTestDAO> LaboratoryTests { get; set; }
    }
}
