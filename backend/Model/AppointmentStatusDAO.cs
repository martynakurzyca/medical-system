﻿using System.Collections.Generic;

namespace MedicalSystemBackend.Model
{
    public class AppointmentStatusDAO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<AppointmentDAO> Appointments { get; set; }
    }
}
