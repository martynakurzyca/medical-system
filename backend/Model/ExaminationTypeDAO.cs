﻿using System.Collections.Generic;

namespace MedicalSystemBackend.Model
{
    public class ExaminationTypeDAO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ExaminationDAO> Examinations { get; set; }
    }
}
