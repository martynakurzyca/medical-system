﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalSystemBackend.Model
{
    public class AdminDAO
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        public UserDAO User { get; set; }
    }
}
