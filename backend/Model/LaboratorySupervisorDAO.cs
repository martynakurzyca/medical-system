﻿using System.Collections.Generic;

namespace MedicalSystemBackend.Model
{
    public class LaboratorySupervisorDAO
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public UserDAO User { get; set; }

        public List<LaboratoryTestDAO> LaboratoryTests { get; set; }
    }
}
