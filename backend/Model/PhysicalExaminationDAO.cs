﻿namespace MedicalSystemBackend.Model
{
    public class PhysicalExaminationDAO
    {
        public int Id { get; set; }
        public string Result { get; set; }

        public int AppointmentId { get; set; }
        public AppointmentDAO Appointment { get; set; }

        public string ExaminationCode { get; set; }
        public ExaminationDAO Examination { get; set; }
    }
}
