﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace MedicalSystemBackend.Controllers
{
    [ApiController]
    [Route("api/appointment")]
    public class AppointmentController : ControllerBase
    {
        private readonly IUserProviderService userProviderService;
        private readonly IUserService userService;
        private readonly IAppointmentService appointmentService;
        public AppointmentController(IUserProviderService userProviderService,
            IUserService userService,
            IAppointmentService appointmentService)
        {
            this.userService = userService;
            this.userProviderService = userProviderService;
            this.appointmentService = appointmentService;
        }
        [Authorize]
        [HttpGet("registrant/getappointments")]
        public IActionResult GetAppointments([FromQuery] int day, [FromQuery] int month, [FromQuery] int year)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsRegistrant(userId))
            {

                if (day == 0 || month == 0 || year == 0)
                {
                    return BadRequest("day, month i year nie moga byc rowne 0");
                }
                DateTime date = new DateTime(year, month, day);
                AppointmentListDTO appointmentListDTO = appointmentService.GetAppointmentsByDate(date);
                return Ok(appointmentListDTO);

            }
            else
            {
                return BadRequest("Brak uprawnien rejestratora");
            }
        }

        [Authorize]
        [HttpGet("doctor/getappointments")]
        public IActionResult GetDoctorsAppointments([FromQuery] int day, [FromQuery] int month, [FromQuery] int year)
        {
            int userId = userProviderService.GetUserId();

            try
            {
                int doctorId = userService.GetDoctorIdByUserId(userId);

                if (day == 0 || month == 0 || year == 0)
                {
                    return BadRequest("day, month i year nie moga byc rowne 0");
                }
                DateTime date = new DateTime(year, month, day);
                AppointmentListDTO appointmentListDTO = appointmentService.GetDoctorsAppointmentsByDate(date, doctorId);
                return Ok(appointmentListDTO);
            }
            catch(MedicalSystemServerException exeption)
            {
                return BadRequest(exeption.Message);
            }

        }
    }
}
