using MedicalSystemBackend.Model;
using MedicalSystemBackend.Repositories;
using MedicalSystemBackend.Services;
using MedicalSystemBackend.ServicesCore;
using MedicalSystemBackend.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace MedicalSystemBackend
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMvc();

            DevelopmentSettings devSettings = new DevelopmentSettings();
            Configuration.GetSection("DevelopmentSettings").Bind(devSettings);
            if (devSettings.IsDevelopment)
            {
                services.AddMvc(option =>
                {
                    option.EnableEndpointRouting = false;
                    option.Filters.Add(new AllowAnonymousFilter());
                });
            }
            else
            {
                services.AddMvc(option =>
                {
                    option.EnableEndpointRouting = false;
                });
            }

            services.AddCors(options =>
            {
                CorsSettings corsSettings = new CorsSettings();
                Configuration.GetSection("CorsSettings").Bind(corsSettings);

                options.AddPolicy(
                    "default",
                    builder =>
                    {
                        builder
                        .WithOrigins(corsSettings.Origins)
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                    });
            });

            TokenSettings tokenSettings = new TokenSettings();
            Configuration.GetSection("TokenSettings").Bind(tokenSettings);

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "JwtBearer";
                options.DefaultChallengeScheme = "JwtBearer";
            }).AddJwtBearer("JwtBearer", jwtOptions =>
            {
                jwtOptions.TokenValidationParameters = new TokenValidationParameters()
                {
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenSettings.SecretKey)),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromHours(5),
                };
            });

            services.AddTransient<IUserProviderService, UserProviderService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ITokenGeneratorService, TokenGeneratorService>();
            services.AddTransient<IUserRoleService, UserRoleService>();
            services.AddTransient<IAppointmentService, AppointmentService>();
            // tu dodajemy serwisy
            services.AddHttpContextAccessor();
            services.AddSingleton<ITokenSettings>(tokenSettings);
            services.AddSingleton<IDevelopmentSettings>(devSettings);


            services.AddDbContextPool<MedicalSystemDbContext>(options => options.UseSqlServer(this.Configuration.GetConnectionString("MedicalSystemDBConnection")));
            services.AddScoped<IRepository<AppointmentDAO>, Repository<AppointmentDAO>>();
            services.AddScoped<IRepository<AppointmentStatusDAO>, Repository<AppointmentStatusDAO>>();
            services.AddScoped<IRepository<DoctorDAO>, Repository<DoctorDAO>>();
            services.AddScoped<IRepository<ExaminationDAO>, Repository<ExaminationDAO>>();
            services.AddScoped<IRepository<ExaminationTypeDAO>, Repository<ExaminationTypeDAO>>();
            services.AddScoped<IRepository<LaboratoryAssistantDAO>, Repository<LaboratoryAssistantDAO>>();
            services.AddScoped<IRepository<LaboratorySupervisorDAO>, Repository<LaboratorySupervisorDAO>>();
            services.AddScoped<IRepository<LaboratoryTestDAO>, Repository<LaboratoryTestDAO>>();
            services.AddScoped<IRepository<LaboratoryTestStatusDAO>, Repository<LaboratoryTestStatusDAO>>();
            services.AddScoped<IRepository<PatientDAO>, Repository<PatientDAO>>();
            services.AddScoped<IRepository<PhysicalExaminationDAO>, Repository<PhysicalExaminationDAO>>();
            services.AddScoped<IRepository<RegistrantDAO>, Repository<RegistrantDAO>>();
            services.AddScoped<IRepository<AdminDAO>, Repository<AdminDAO>>();
            services.AddScoped<IRepository<UserDAO>, Repository<UserDAO>>();
            services.AddScoped<IRepository<UserRoleDAO>, Repository<UserRoleDAO>>();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseAuthentication();
            app.UseCors("default");
            app.UseRouting();
            app.UseMvc();
        }
    }
}
