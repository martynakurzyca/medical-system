﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalSystemBackend.DTO
{
    public class UserDataDTO
    {
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Role { get; set; }
    }
}
