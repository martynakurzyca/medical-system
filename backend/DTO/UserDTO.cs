﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalSystemBackend.DTO
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public bool IsActive { get; set; }
        public RoleDTO Role { get; set; }
        //wypelnic tylko w przypadku doktora
        public string LicenceNumber { get; set; }
    }
}
