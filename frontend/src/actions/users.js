import axiosDefault from "./axiosConfiguration";

function getUsers() {
  return axiosDefault({
    method: "GET",
    url: "/api/user/admin/allusers",
  });
}

function getLoggedUserData() {
  return axiosDefault({
    method: "GET",
    url: "/api/user/myProfile"
  });
}

export { getUsers, getLoggedUserData }