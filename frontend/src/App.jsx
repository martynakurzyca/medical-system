import React, { Component } from 'react';
import { AppBar, Toolbar, Typography, IconButton } from '@material-ui/core';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { Route } from 'react-router-dom';
import UsersContainer from './components/UsersContainer';
import Footer from './ui/Footer';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: props.user,
    };
  }

  render() {
    return (
      <div className="app">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
        <AppBar position="sticky" className="navbar">
          <Toolbar>
            <Typography variant="h5" style={{ marginLeft: '1%' }}>
              medical system
            </Typography>
            <IconButton className="right">
              <ExitToAppIcon className="button" />
            </IconButton>
          </Toolbar>
        </AppBar>
        <div className="content">
          <Route path='/users' component={UsersContainer} />
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
