import React, { Component } from 'react';
import UsersComponent from './UsersComponent';
import { getUsers } from './../actions/users';

class UsersContainer extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
    };
  }

  componentDidMount() {
    getUsers()
      .then(res => {
        console.log(res);
        this.setState({
          users: res.data.users,
        });
      });
  }

  render() {
    return (
      <UsersComponent
        columns={[
          {
            title: 'Login',
            field: 'login',
          },
          {
            title: 'Imię',
            field: 'firstname',
          },
          {
            title: 'Nazwisko',
            field: 'surname',
          },
          {
            title: 'Rola',
            field: 'role.name',
          },
          {
            title: 'Aktywny',
            field: 'isActive',
            type: 'boolean',
          },
        ]}
        data={this.state.users}
      />
    )
  }
}

export default UsersContainer;