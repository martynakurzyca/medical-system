import React from 'react';
import MaterialTable from 'material-table';
import { Card, Typography } from '@material-ui/core';

const UsersComponent = (props) => {
  return (
    <Card className="card">
      <Typography variant="h5" className="underline-title">
        Użytkownicy
      </Typography>
      <div className="table">
        <MaterialTable
          columns={props.columns}
          data={props.data}
          title="Użytkownicy"
          options={{
            emptyRowsWhenPaging: false,
          }}
        />
      </div>
    </Card>
  );
}

export default UsersComponent;