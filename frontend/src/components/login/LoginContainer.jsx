import React, { Component } from 'react';
import LoginComponent from './LoginComponent';
import App from './../../App';
import { login } from './../../actions/auth';
import { withRouter } from 'react-router-dom';

class LoginContainer extends Component {
  constructor(props) { 
    super();
  }

  redirectToMainPage = () => {
    this.props.history.push("/");
  }

  log = values => login(values);


  render() {
    return (
      <LoginComponent 
        log={this.log}
        redirectToMainPage={this.redirectToMainPage}
      />
    )
  }
}

export default withRouter(LoginContainer);