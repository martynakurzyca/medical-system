import React from 'react';
import { Card, TextField, Grid, Typography, Button } from '@material-ui/core'
import { withFormik } from 'formik';
import { withSnackbar } from './../../ui/SnackbarContext';

const formikEnhancer = withFormik({
  enableReinitialize: true,

  mapPropsToValues: props => ({
    Login: '',
    Password: '',
  }),

  handleSubmit: (values, { props }) => {
      props.log(values)
        .then(() => {
          props.redirectToMainPage();
        })
        .catch(error => {
          props.showMessage(error.response.data);
        });
    }
  });

const LoginComponent = (props) => (
  <>
    <Card className='login-card box-center'>
      <Typography variant="h5" className="underline-title-login">
        medical system
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <TextField
            className='input'
            id='Login'
            onChange={props.handleChange}
            fullWidth
            variant='outlined'
            label='Login'
            style={{ marginTop: '1%' }}
            value={props.values.Login}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            className='input'
            id='Password'
            onChange={props.handleChange}
            fullWidth
            variant='outlined'
            label='Hasło'
            type="password"
            value={props.values.Password}
          />
        </Grid>
        <Grid item xs={0} md={6}/>
        <Grid item xs={12} md={6}>
        <Button color="primary" fullWidth onClick={props.handleSubmit}>
          Zaloguj
        </Button>
        </Grid>
      </Grid>
    </Card>
  </>
);

export default withSnackbar(formikEnhancer(LoginComponent));